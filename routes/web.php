<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['verified'])->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/edit-profile', [App\Http\Controllers\HomeController::class, 'edit']);

    Route::get('/item', [App\Http\Controllers\ItemController::class, 'index']);
    Route::get('/item-edit/{id}', [App\Http\Controllers\ItemController::class, 'edit']);
    Route::get('/item-delete/{id}', [App\Http\Controllers\ItemController::class, 'destroy']);
    Route::get('/item-export', [App\Http\Controllers\ItemController::class, 'export_excel']);
    Route::get('/item-export-pdf', [App\Http\Controllers\ItemController::class, 'export_pdf']);

    Route::get('/toko', [App\Http\Controllers\TokoController::class, 'index']);
    Route::get('/toko-edit/{id}', [App\Http\Controllers\TokoController::class, 'edit']);
    Route::get('/toko-delete/{id}', [App\Http\Controllers\TokoController::class, 'destroy']);
    Route::get('/toko-export', [App\Http\Controllers\TokoController::class, 'export_excel']);
    Route::get('/toko-export-pdf', [App\Http\Controllers\TokoController::class, 'export_pdf']);
    Route::get('/download-toko', [App\Http\Controllers\TokoController::class, 'download']);

    Route::get('/transaction', [App\Http\Controllers\TransactionController::class, 'index']);
    Route::get('/transaction/editHarga/{id}', [App\Http\Controllers\TransactionController::class, 'editHarga']);
    Route::get('/transaction/detail/{id}', [App\Http\Controllers\TransactionController::class, 'show'])->where('id', '.*');
    Route::get('/transaction-delete/{id}', [App\Http\Controllers\TransactionController::class, 'destroy']);
    Route::get('/transaction-export', [App\Http\Controllers\TransactionController::class, 'export_excel']);
    Route::get('/transaction-export-pdf/{id}', [App\Http\Controllers\TransactionController::class, 'export_pdf'])->where('id', '.*');
    Route::get('/download-transaction', [App\Http\Controllers\TransactionController::class, 'download']);
    
    Route::post('/update-profile', [App\Http\Controllers\HomeController::class, 'update']);
    Route::post('/update-item', [App\Http\Controllers\ItemController::class, 'update']);
    Route::post('/update-toko', [App\Http\Controllers\TokoController::class, 'update']);
    Route::post('/item/store', [App\Http\Controllers\ItemController::class, 'store']);
    Route::post('/toko/store', [App\Http\Controllers\TokoController::class, 'store']);
    Route::post('/transaction/editHarga', [App\Http\Controllers\TransactionController::class, 'updateHarga']);
    Route::post('/transaction/store', [App\Http\Controllers\TransactionController::class, 'store']);
    Route::post('/update-transaction', [App\Http\Controllers\TransactionController::class, 'update']);
    Route::post('/item/import_excel', [App\Http\Controllers\ItemController::class, 'import_excel']);
    Route::post('/toko/import_excel', [App\Http\Controllers\TokoController::class, 'import_excel']);
    Route::post('/transaction/import_excel', [App\Http\Controllers\TransactionController::class, 'import_excel']);
    Route::post('/transaction/export_excel', [App\Http\Controllers\TransactionController::class, 'export_excel']);
});