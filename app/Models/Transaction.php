<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    public function toko()
    {
        return $this->belongsTo(Toko::class);
    }

    protected $fillable = [
        'toko_id',
        'nomor_faktur',
        'tanggal',
        'item_id',
        'nama_item',
        'satuan',
        'qty',
        'ppn',
        'harga',
        'tipe',
    ];
}
