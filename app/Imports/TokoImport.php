<?php

namespace App\Imports;

use App\Models\Toko;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class TokoImport implements ToModel, WithStartRow
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        return new Toko([
            'name'        => $row[0],
            'phone'       => $row[1],
            'address'     => $row[2],
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
