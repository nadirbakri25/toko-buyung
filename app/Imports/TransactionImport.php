<?php

namespace App\Imports;

use App\Models\LogFaktur;
use App\Models\Transaction;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class TransactionImport implements ToModel, WithStartRow, WithChunkReading
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        LogFaktur::firstOrCreate(['nomor_faktur' => $row[1], 'toko_id' => $row[0]]);

        return new Transaction([
            'toko_id'       => $row[0],
            'nomor_faktur'  => $row[1],
            'tanggal'       => \Carbon\Carbon::parse($row[2]),
            'item_id'       => $row[4],
            'nama_item'     => $row[3],
            'satuan'        => $row[5],
            'qty'           => $row[6],
            'ppn'           => $row[7]*0.1,
            'harga'         => $row[7],
            'tipe'          => $row[8],
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
