<?php

namespace App\Imports;

use App\Models\Item;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ItemImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Item([
            'id'        => $row[0],
            'nama_item' => $row[1],
            'qty'       => $row[2],
            'harga'     => $row[3],
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
