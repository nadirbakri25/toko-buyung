<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\LogFaktur;
use App\Models\Toko;
use App\Models\Transaction;
use Illuminate\Http\Request;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {        
        $items = Toko::all()->take(10);

        $transactions = LogFaktur::with('toko')->paginate(15);

        $barang = Item::all()->take(10);

        return view('home', compact('items', 'transactions', 'barang'));
    }

    public function edit()
    {   
        return view('editUser');
    }

    public function update(Request $request)
    {   
        $request->validate([
            'name'         => 'required|max:255|min:3',
            'email'        => 'required|max:255|min:3',
        ]);
        
        $user = auth()->user();
        $user->update([
            'name'  => $request->input('name'),
            'email' => $request->input('email'),
        ]);

        return redirect('/edit-profile');
    }
}
