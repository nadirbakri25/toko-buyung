<?php

namespace App\Http\Controllers;

use App\Exports\TransactionExport;
use App\Imports\TransactionImport;
use App\Models\Item;
use App\Models\LogFaktur;
use App\Models\Toko;
use App\Models\Transaction;
use Illuminate\Http\Request;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

class TransactionController extends Controller
{
    public function index()
    {
        $log = LogFaktur::orderBy('id', 'desc')->with('toko')->paginate(15);
        $allToko = Toko::all();
        
        return view('transaction', compact('log', 'allToko'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'toko_id'       => 'required',
            'nomor_faktur'  => 'required',
            'tanggal'       => 'required',
            'item_id'       => 'required',
            'nama_item'     => 'required',
            'satuan'        => 'required',
            'qty'           => 'required',
            'harga'         => 'required',
            'tipe'          => 'required',
        ]);

        Transaction::create([
            'toko_id'       => $request->toko_id,
            'nomor_faktur'  => $request->nomor_faktur,
            'tanggal'       => $request->tanggal,
            'item_id'       => $request->item_id,
            'nama_item'     => $request->nama_item,
            'satuan'        => $request->satuan,
            'qty'           => $request->qty,
            'ppn'           => ($request->qty*0.1),
            'harga'         => $request->harga,
            'tipe'          => $request->tipe
        ]);

        LogFaktur::create([
            'nomor_faktur' => $request->nomor_faktur,
            'toko_id'      => $request->toko_id
        ]);

        return redirect('/transaction');
    }

    public function show($id)
    {
        $transactions = Transaction::where('nomor_faktur', $id)->get();

        if ($transactions == null) {
            abort(404);
        }

        return view('showTransaction', compact('transactions'));
    }

    // public function update(Request $request)
    // {
    //     $request->validate([
    //         'toko_id'       => 'required',
    //         'nomor_faktur'  => 'required',
    //         'tanggal'       => 'required',
    //         'item_id'       => 'required',
    //         'nama_item'     => 'required',
    //         'satuan'        => 'required',
    //         'qty'           => 'required',
    //         'harga'         => 'required',
    //         'tipe'          => 'required'
    //     ]);

    //     $transactions = Transaction::find($request->id);

    //     $transactions->update([
    //         'toko_id'       => $request->toko_id,
    //         'nomor_faktur'  => $request->nomor_faktur,
    //         'tanggal'       => $request->tanggal,
    //         'item_id'       => $request->item_id,
    //         'nama_item'     => $request->nama_item,
    //         'satuan'        => $request->satuan,
    //         'qty'           => $request->qty,
    //         'harga'         => $request->harga,
    //         'tipe'          => $request->tipe
    //     ]);

    //     return redirect('/transaction');
    // }

    public function destroy($id)
    {
        $log = LogFaktur::find($id);

        LogFaktur::find($id)->delete();
        Transaction::where('nomor_faktur', $log->nomor_faktur)->delete();
        return redirect('/transaction');
    }

    public function import_excel(Request $request)
    {        
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');
        $fileName = rand().$file->getClientOriginalName();
        $file->move('file_item', $fileName);

        Excel::import(new TransactionImport, public_path('/file_item/'.$fileName));

        $transaction = Transaction::all()->toArray();
        foreach ($transaction as $value) {
            $item = Item::find($value['item_id']);
            
            if ($item == null) {
                Item::create([
                    'id'        => $value['item_id'],
                    'nama_item' => $value['nama_item'],
                    'qty'       => 0,
                    'satuan'    => $value['satuan'],
                    'harga'     => $value['harga'],
                ]);
            }
        }

        return redirect('/transaction');
    }

    public function export_excel(Request $request)
    {
        $tanggal['selesai'] = $request->tanggal_selesai;
        $tanggal['mulai'] = $request->tanggal_mulai;
        return \Excel::download(new TransactionExport($tanggal), 'transaction.xlsx');
    }

    public function export_pdf($id)
    {
        $transactions = Transaction::where('nomor_faktur', $id)->get();
        $total[0] = 0;
        $total[1] = 0;

        foreach ($transactions as $value) {
            $total[0] = $total[0]+$value->qty;
            $total[1] = $total[1]+($value->harga*$value->qty);
        }
 
    	$pdf = PDF::loadview('exports.transactions',['transactions'=>$transactions], ['total'=>$total])->setPaper('a4', 'landscape');
    	return $pdf->download('transaction.pdf');
    }

    public function download()
    {
        $path = public_path('template_import/transaksi.xlsx');
        $headers = array(
            'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        );
        return response()->download($path, 'transaksi.xlsx', $headers);
    }

    public function editHarga($id)
    {
        $item = Transaction::where('item_id', $id)->first();
        
        return view('editHarga', compact('item'));
    }
    public function updateHarga(Request $request)
    {
        $request->validate([
            'harga' => 'required',
        ]);

        Transaction::where('item_id', $request->id)->update(['harga' => $request->harga]);
        Item::find($request->id)->update(['harga' => $request->harga]);
        
        return redirect('/transaction');
    }
}
