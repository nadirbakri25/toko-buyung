<?php

namespace App\Http\Controllers;

use PDF;
use App\Exports\TokoExport;
use App\Imports\TokoImport;
use App\Models\Toko;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TokoController extends Controller
{
    public function index()
    {
        $tokos = Toko::orderBy('id', 'DESC')->paginate(15);
        return view('toko', compact('tokos'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'            => 'required|max:255|min:3',
            'phone'           => 'required',
            'address'         => 'required'
        ]);

        Toko::create([
            'name'            => $request->name,
            'phone'           => $request->phone,
            'address'         => $request->address
        ]);

        return redirect('/toko');
    }

    public function edit($id)
    {
        $toko = Toko::find($id);

        if ($toko == null) {
            abort(404);
        }

        return view('editToko', compact('toko'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'name'           => 'required|max:255|min:3',
            'phone'          => 'required|max:255|min:3',
            'address'        => 'required|max:255|min:3',
        ]);

        $toko = Toko::find($request->id);

        $toko->update([
            'name'           => $request->name,
            'phone'          => $request->phone,
            'address'        => $request->address,
        ]);

        return redirect('/toko');
    }

    public function destroy($id)
    {
        Toko::find($id)->delete();
        return redirect('/toko');
    }

    public function import_excel(Request $request)
    {        
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');
        $fileName = rand().$file->getClientOriginalName();
        $file->move('file_item', $fileName);

        Excel::import(new TokoImport, public_path('/file_item/'.$fileName));

        return redirect('/toko');
    }

    public function download()
    {
        $path = public_path('template_import/toko.xlsx');
        $headers = array(
            'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        );
        return response()->download($path, 'toko.xlsx', $headers);
    }
}
    