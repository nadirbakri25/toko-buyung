<?php

namespace App\Http\Controllers;

use App\Exports\ItemExport;
use Session;
use PDF;
use App\Models\Item;
use App\Imports\ItemImport;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ItemController extends Controller
{
    public function index()
    {
        $items = Item::orderBy('id', 'DESC')->paginate(15);
        return view('item', compact('items'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_item'     => 'required|max:255|min:3',
            'qty'           => 'required',
            'harga'         => 'required'
        ]);

        Item::create([
            'nama_item'     => $request->nama_item,
            'qty'           => $request->qty,
            'harga'         => $request->harga
        ]);

        return redirect('/item');
    }

    public function edit($id)
    {
        $item = Item::find($id);

        if ($item == null) {
            abort(404);
        }

        return view('editItem', compact('item'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'harga'        => 'required|max:255|min:3',
        ]);

        $item = Item::find($request->id);

        $transaction = Transaction::where('item_id', $item->id);

        $item->update([
            'harga'        => $request->harga,
        ]);

        $transaction->update([
            'harga'        => $request->harga,
        ]);

        return redirect('/item');
    }

    public function destroy($id)
    {
        Item::find($id)->delete();
        return redirect('/item');
    }

    public function import_excel(Request $request)
    {        
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');
        $fileName = rand().$file->getClientOriginalName();
        $file->move('file_item', $fileName);

        Excel::import(new ItemImport, public_path('/file_item/'.$fileName));

        Session::flash('Sukses', 'Data Sukses di Import!');
        return redirect('/item');
    }

    public function export_excel()
    {
        return \Excel::download(new ItemExport, 'item.xlsx');
    }

    public function export_pdf()
    {
        $item = Item::all();
 
    	$pdf = PDF::loadview('exports.item_pdf',['item'=>$item]);
    	return $pdf->download('item-pdf.pdf');
    }
}
