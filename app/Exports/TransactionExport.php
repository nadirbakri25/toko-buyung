<?php

namespace App\Exports;

use App\Models\Transaction;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TransactionExport implements FromQuery, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $tanggal;

    public function __construct($tanggal) {
        $this->tanggal = $tanggal;
    }

    public function query()
    {
        return Transaction::query()->whereBetween('tanggal',[ $this->tanggal['mulai'], $this->tanggal['selesai']]);
    }

    public function headings(): array
    {
        return ["Kode Toko", "Nomor Faktur", "Tanggal", "Nama Barang", "Kode Item", "Satuan", "Qty", "Harga Satuan", "PPN", "Cash / Credit"];
    }

    public function map($row): array{
        $date = date('d/m/Y', strtotime($row->tanggal));

        $fields = [
            $row->toko->id,
            $row->nomor_faktur,
            $date,
            $row->nama_item,
            $row->item_id,
            $row->satuan,
            $row->qty,
            $row->harga,
            ($row->harga*0.1),
            $row->tipe,
        ];
     return $fields;
    }
}
