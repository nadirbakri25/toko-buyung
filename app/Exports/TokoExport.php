<?php

namespace App\Exports;

use App\Models\Toko;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TokoExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Toko::all();
    }

    public function map($row): array{
        $fields = [
            $row->name,
            $row->phone,
            $row->address,
        ];
     return $fields;
 }

    public function headings(): array
    {
        return ["Nama Toko", "Nomor Telpon", "Alamat Toko"];
    }
}
