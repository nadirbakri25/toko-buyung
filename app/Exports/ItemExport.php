<?php

namespace App\Exports;

use App\Models\Item;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ItemExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Item::all();
    }

    public function map($row): array{
        $fields = [
            $row->id,
            $row->nama_item,
            $row->qty,
            (int)($row->harga)*1000,
        ];
     return $fields;
 }

    public function headings(): array
    {
        return ["ID", "Nama Item", "Qty", "Harga"];
    }
}
