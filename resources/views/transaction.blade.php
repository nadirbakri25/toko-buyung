@extends('layouts.main-app')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h4>Data Transaction</h4>
            <div class="card-header-action">
                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addData">Tambah Data</button> --}}
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#importExcel">Import Excel</button>
                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#buatReport" >Buat Report</button>
                <a href="/download-transaction" type="button" class="btn btn-danger">Download Template Import</a>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-hover text-center">                
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nomor Faktur</th>
                        <th scope="col">Nama Toko</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($log as $items)
                        <tr>
                            <th scope="row">{{ $items->id }}</th>
                            <td>{{ $items->nomor_faktur }}</td>
                            <td>{{ $items->toko->name }}</td>
                            <td>
                                <a href="/transaction/detail/{{ $items->nomor_faktur }}" class="badge badge-primary">Detail</a>
                                <a href="/transaction-export-pdf/{{ $items->nomor_faktur }}" class="badge badge-dark">Get Invoice</a>
                                <a href="/transaction-delete/{{ $items->id }}" class="badge badge-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="align-items:center; display: flex; justify-content: center">
                {{ $log->links() }}
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="addData" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="/transaction/store">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addData">Tambah Data</h5>
                    </div>
                    <div class="modal-body">

                        @csrf

                        <div class="row">
                            <div class="form-group col">
                                <label>Nama Toko</label>
                                <select class="custom-select" name="toko_id" required>
                                    @foreach ($allToko as $toko)
                                        <option value="{{ $toko->id }}">{{ $toko->name }}</option>       
                                    @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Nomor Faktur</label>
                                <input type="text" name="nomor_faktur" class="form-control" value="{{ old('nomor_faktur') }}" required="">
                                @error('nomor_faktur')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Tanggal</label>
                                <input type="date" name="tanggal" class="form-control" value="{{ old('tanggal') }}" required="">
                                @error('tanggal')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Item ID</label>
                                <input type="text" name="item_id" class="form-control" value="{{ old('item_id') }}" required="">
                                @error('item_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Nama Item</label>
                                <input type="text" name="nama_item" class="form-control" value="{{ old('nama_item') }}" required="">
                                @error('nama_item')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Satuan</label>
                                <input type="text" name="satuan" class="form-control" value="{{ old('satuan') }}" required="">
                                @error('satuan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Qty</label>
                                <input type="number" name="qty" class="form-control" value="{{ old('qty') }}" required="">
                                @error('qty')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Harga</label>
                                <input type="number" name="harga" class="form-control" value="{{ old('harga') }}" required="">
                                @error('harga')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Tipe</label>
                                <select class="custom-select" name="tipe" required>
                                    <option value="CASH">Cash</option>       
                                    <option value="CREDIT">Credit</option>       
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="importExcel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="/transaction/import_excel" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="importExcel">Import Excel</h5>
                    </div>
                    <div class="modal-body">

                        {{ csrf_field() }}

                        <label>Pilih file excel</label> 
                        <div class="form-group">
                            <input type="file" name="file" required="required">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="buatReport" tabindex="-1" role="dialog" aria-labelledby="buatReport" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="/transaction/export_excel">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="buatReport">Buat Report</h5>
                    </div>
                    <div class="modal-body">

                        {{ csrf_field() }}

                        <div class="row">
                            <div class="form-group col">
                                <label>Tanggal Mulai</label>
                                <input type="date" name="tanggal_mulai" class="form-control" required="">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col">
                                <label>Tanggal Selesai</label>
                                <input type="date" name="tanggal_selesai" class="form-control" required="">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Buat Report</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
