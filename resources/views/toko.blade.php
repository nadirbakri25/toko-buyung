@extends('layouts.main-app')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h4>Data Toko</h4>
            <div class="card-header-action">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addData">Tambah Data</button>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#importExcel">Import Excel</button>
                <a href="/download-toko" type="button" class="btn btn-danger">Download Template Import</a>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nama Toko</th>
                        <th scope="col">No Telp</th>
                        <th scope="col">Alamat Toko</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tokos as $item)
                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->phone }}</td>
                        <td>{{ $item->address }}</td>
                        <td>
                            <a href="/toko-edit/{{ $item->id }}" class="badge badge-primary">Edit</a>
                            <a href="/toko-delete/{{ $item->id }}" class="badge badge-danger">Delete</a>
                        </td>
                    </tr>                                        
                    @endforeach
                </tbody>
            </table>
            <div style="align-items:center; display: flex; justify-content: center">
                {{ $tokos->links() }}
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="importExcel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="/toko/import_excel" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="importExcel">Import Excel</h5>
                    </div>
                    <div class="modal-body">

                        {{ csrf_field() }}

                        <label>Pilih file excel</label>
                        <div class="form-group">
                            <input type="file" name="file" required="required">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="addData" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="/toko/store">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addData">Tambah Data</h5>
                    </div>
                    <div class="modal-body">

                        @csrf

                        <div class="row">
                            <div class="form-group col">
                                <label>Nama Toko</label>
                                <input name="name" type="text" class="form-control" value="{{ old('name') }}" required="">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Nomor Telpon</label>
                                <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" required="">
                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Alamat</label>
                                <input type="text" name="address" class="form-control" value="{{ old('address') }}" required="">
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
