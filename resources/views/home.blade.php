@extends('layouts.main-app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Tabel Toko</h4>
            <div class="card-header-action">
                <a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-collapse">
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Toko</th>
                            <th scope="col">Nomor Telpon</th>
                            <th scope="col">Alamat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->phone }}</td>
                            <td>{{ $item->address }}</td>
                        </tr>                                        
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <a href="/toko">Klik disini untuk melihat lebih lanjut</a>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h4>Tabel Transaksi</h4>
            <div class="card-header-action">
                <a data-collapse="#mycard-collapse2" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-collapse2">
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nomor Faktur</th>
                            <th scope="col">Nama Toko</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transactions as $items)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $items->nomor_faktur }}</td>
                                <td>{{ $items->toko->name }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <a href="/transaction">Klik disini untuk melihat lebih lanjut</a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4>Tabel Item</h4>
            <div class="card-header-action">
                <a data-collapse="#mycard-collapse2" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-collapse2">
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Item</th>
                            <th scope="col">Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($barang as $items)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $items->nama_item }}</td>
                                <td>{{ $items->harga }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <a href="/item">Klik disini untuk melihat lebih lanjut</a>
            </div>
        </div>
    </div>
@endsection
