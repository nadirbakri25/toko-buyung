@extends('layouts.main-app')

@section('content')
    <div class="section-header">
        <h1>Transaction</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="/home">Dashboard</a></div>
            <div class="breadcrumb-item">Transaction</div>
        </div>
    </div>
    <div class="section-body">
        <h2 class="section-title">Hi, {{ Auth::user()->name }}</h2>
        <p class="section-lead">
            Change information about the Transaction.
        </p>

        <div class="row mt-sm-4">
            <div class="col">
                <div class="card">
                    <form method="post" action="/update-transaction" class="needs-validation">
                        @csrf
                        <input name="id" type="hidden" class="form-control" value="{{ $transactions->id }}" required="">
                        <div class="card-header">
                            <h4>Edit Transaksi</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col">
                                    <label>Name Toko (Sebelumnya : {{ $transactions->toko->name }})</label>
                                    <select class="custom-select" name="toko_id" required>
                                        @foreach ($allToko as $toko)
                                            <option value="{{ $toko->id }}">{{ $toko->name }}</option>       
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <label>Nomor Faktur</label>
                                    <input type="text" name="nomor_faktur" class="form-control" value="{{ $transactions->nomor_faktur }}" required="">
                                    @error('nomor_faktur')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <label>Tanggal (Sebelumnya : {{ $transactions->tanggal }})</label>
                                    <input type="date" name="tanggal" class="form-control" value="{{ old('tanggal') }}" required="">
                                    @error('tanggal')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <label>Item ID</label>
                                    <input type="text" name="item_id" class="form-control" value="{{ $transactions->item_id }}" required="">
                                    @error('item_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <label>Nama Item</label>
                                    <input type="text" name="nama_item" class="form-control" value="{{ $transactions->nama_item }}" required="">
                                    @error('nama_item')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <label>Satuan</label>
                                    <input type="text" name="satuan" class="form-control" value="{{ $transactions->satuan }}" required="">
                                    @error('satuan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <label>Qty</label>
                                    <input type="number" name="qty" class="form-control" value="{{ $transactions->qty }}" required="">
                                    @error('qty')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <label>Harga</label>
                                    <input type="number" name="harga" class="form-control" value="{{ $transactions->harga }}" required="">
                                    @error('harga')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <label>Tipe (Sebelumnya : {{ $transactions->tipe }})</label>
                                    <select class="custom-select" name="tipe" required>
                                        <option value="CASH">Cash</option>       
                                        <option value="CREDIT">Credit</option>       
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
