@extends('layouts.main-app')

@section('content')
    <div class="section-header">
        <h1>Transaksi - {{ $transactions[0]->nomor_faktur }}</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="/home">Dashboard</a></div>
            <div class="breadcrumb-item">Transaction</div>
        </div>
    </div>
    <div class="section-body">
        <h2 class="section-title">Hi, {{ Auth::user()->name }}</h2>
        <p class="section-lead">
            Cek detail transaksi dibawah    
        </p>

        <div class="row mt-sm-4">
            <div class="col">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Transaction Detail</h4>
                        <div class="card-header-action">
                            <a href="/transaction-export-pdf/{{ $transactions[0]->nomor_faktur }}" type="button" class="btn btn-dark">Download Invoice</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col">
                                <label>Nama Toko</label>
                                <input type="text" name="nomor_faktur" class="form-control" value="{{ $transactions[0]->toko->name }}" disabled>                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Nomor Faktur</label>
                                <input type="text" name="nomor_faktur" class="form-control" value="{{ $transactions[0]->nomor_faktur }}" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label>Tanggal</label>
                                <input type="text" name="tanggal" class="form-control" value="{{ $transactions[0]->tanggal }}" disabled>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col">
                                <label>List Item</label>
                                <table class="table table-hover text-center">
                                    <thead>
                                        <tr>
                                            <th scope="col">Item ID</th>
                                            <th scope="col">Nama Item</th>
                                            <th scope="col">Satuan</th>
                                            <th scope="col">Qty</th>
                                            <th scope="col">Harga</th>
                                            <th scope="col">PPN</th>
                                            <th scope="col">Tipe</th>
                                            <th scope="col">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($transactions as $item)
                                        <tr>
                                            <th scope="row">{{ $item->item_id }}</th>
                                            <td>{{ $item->nama_item }}</td>
                                            <td>{{ $item->satuan }}</td>
                                            <td>{{ $item->qty }}</td>
                                            <td>{{ $item->harga }}</td>
                                            <td>{{ $item->harga*0.1 }}</td>
                                            <td>{{ $item->tipe }}</td>
                                            <td><a href="/transaction/editHarga/{{ $item->item_id }}" class="badge badge-primary">Edit Harga</a></td>
                                        </tr>                                        
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection