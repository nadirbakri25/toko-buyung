<html>
<head>
	<title>TOKO - PDF</title>
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}

        table, td, th {
            border: 1px solid black;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }
	</style>
	<center>
		<h5>Laporan PDF</h4>
    </center>
 
	<table>
		<thead>
			<tr>
				<th>Nama Toko</th>
				<th>Nomor Telpon</th>
				<th>Alamat</th>
			</tr>
		</thead>
		<tbody>
			@foreach($item as $p)
			<tr>
				<td>{{$p->name}}</td>
				<td>{{$p->phone}}</td>
				<td>{{$p->address}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>