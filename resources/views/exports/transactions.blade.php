<!DOCTYPE html>
<html lang="en">

<head>
    <title>Invoice - {{ $transactions[0]->id }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        body{
            padding-left: 5%;
            padding-right: 5%;
        }
        table {
            display: table;
            border-collapse: separate;
            box-sizing: border-box;
            text-indent: initial;
            border-spacing: 0px;
            border-color: gray;
        }
        .border_bottom {
            border-bottom: 3px solid;
            margin: 5px;
        }

        .border_top {
            border-top: 3px solid;
            margin: 5px;
        }

        .bold {
            font-weight: bold;
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-12" style="padding-top: 4%;">
                <center>
                    <h1>BUYUNG BANGKA(sbn)</h1>
                    <h2 style="color: crimson;">Detail Penjualan</h2>

                    <div>
                        <table width="100%">
                            <tr>
                                <td></td>
                                <td style="text-align:right;">Periode Pembelian : 1 Maret 2020 - 2 Maret 2021</td>
                            </tr>
                            <tr>
                                <td class="border_bottom">Mata Uang : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rupiah</td>
                                <td class="border_bottom"></td>
                            </tr>
                            <tr>
                                <td class="border_bottom">Group Customer : </td>
                                <td class="border_bottom"></td>
                            </tr>
                        </table>
                        <table width="100%">
                            <tr>
                                <td class="border_bottom" width="200px"></td>
                                <td class="border_bottom">Nomor Faktur</td>
                                <td class="border_bottom">Cabang</td>
                                <td class="border_bottom">Customer</td>
                                <td class="border_bottom">Sales</td>
                                <td class="border_bottom">Referensi</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>{{ $transactions[0]->nomor_faktur }}</td>
                                <td></td>
                                <td>{{ $transactions[0]->tipe }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <div>
                        <table width="100%">
                            <thead>
								<tr>
									<th>Kode Item</th>
									<th>Nama Item</th>
									<th>Qty</th>
									<th>Satuan</th>
									<th>PPN</th>
									<th>Harga</th>
									<th>Sub Total</th>
								</tr>
                            </thead>
                            <tbody>
                                @foreach ($transactions as $item)
                                <tr>
									<td>{{ $item->item_id }}</td>
                                    <td>{{ $item->nama_item }}</td>
                                    <td>{{ $item->qty }}</td>
                                    <td>{{ $item->satuan }}</td>
                                    <td>{{ ($item->harga*0.1) }}</td>
                                    <td>{{ $item->harga }}</td>
                                    <td>{{ $item->harga*$item->qty }}</td>
                                </tr>
								@endforeach
                                <!-- Loop disini end -->

                                <!-- Total -->
                                <tr>
                                    <td class="bold border_top">
                                    </td>
                                    <td class="bold border_top">
                                        Total Qty :
                                    </td>
                                    <td class="bold border_top">
                                        {{ $total[0] }} (Unit 1)
                                    </td>
                                    <td class="bold border_top">
                                    </td>
                                    <td class="bold border_top">
                                        Subtotal Rp.
                                    </td>
                                    <td class="bold border_top">
                                        {{$total[1]}}
                                    </td>
                                </tr>
                                <tr class="bold">
                                    <td colspan="4">
                                    </td>
                                    <td>
                                        Disc Rp.
                                    </td>
                                    <td>
                                        0
                                    </td>
                                </tr>
                                <tr class="bold">
                                    <td colspan="4">
                                    </td>
                                    <td>
                                        Pajak Rp.
                                    </td>
                                    <td>
                                        0
                                    </td>
                                </tr>
                                <tr class="bold">
                                    <td colspan="4">
                                    </td>
                                    <td>
                                        Freight Rp.
                                    </td>
                                    <td>
                                        0
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                    </td>
                                    <td class="bold border_top">
                                        Total Rp.
                                    </td>
                                    <td class="bold border_top">
                                        {{$total[1]}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </center>
            </div>
        </div>
    </div>

</body>

</html>