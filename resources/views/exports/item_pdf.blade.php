<html>
<head>
	<title>ITEM - PDF</title>
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}

        table, td, th {
            border: 1px solid black;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }
	</style>
	<center>
		<h5>Laporan PDF</h4>
    </center>
 
	<table>
		<thead>
			<tr>
				<th>ID</th>
				<th>Nama Item</th>
				<th>Qty</th>
				<th>Harga</th>
			</tr>
		</thead>
		<tbody>
			@foreach($item as $p)
			<tr>
				<td>{{$p->id }}</td>
				<td>{{$p->nama_item}}</td>
				<td>{{$p->qty}}</td>
				<td>{{$p->harga}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>