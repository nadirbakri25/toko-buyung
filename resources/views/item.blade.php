@extends('layouts.main-app')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h4>Tabel Item</h4>
        </div>
        <div class="card-body">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama Item</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($items as $item)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $item->nama_item }}</td>
                        <td>{{ $item->harga }}</td>
                        <td>
                            <a href="/item-edit/{{ $item->id }}" class="badge badge-primary">Edit Harga</a>
                            <a href="/item-delete/{{ $item->id }}" class="badge badge-danger">Delete</a>
                        </td>
                    </tr>                                        
                    @endforeach
                </tbody>
            </table>
            <div style="align-items:center; display: flex; justify-content: center">
                {{ $items->links() }}
            </div>
        </div>
    </div>
@endsection