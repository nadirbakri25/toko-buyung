@extends('layouts.main-app')

@section('content')
    <div class="section-header">
        <h1>Toko</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="/home">Dashboard</a></div>
            <div class="breadcrumb-item">Toko</div>
        </div>
    </div>
    <div class="section-body">
        <h2 class="section-title">Hi, {{ Auth::user()->name }}</h2>
        <p class="section-lead">
            Change information about the Store.
        </p>

        <div class="row mt-sm-4">
            <div class="col">
                <div class="card">
                    <form method="post" action="/update-toko" class="needs-validation">
                        @csrf
                        <input name="id" type="hidden" class="form-control" value="{{ $toko->id }}" required="">
                        <div class="card-header">
                            <h4>Edit {{ $toko->name }}</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col">
                                    <label>Nama Toko</label>
                                    <input name="name" type="text" class="form-control" value="{{ $toko->name }}" required="">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-7 col-12">
                                    <label>Nomor Telpon</label>
                                    <input type="text" name ="phone" class="form-control" value="{{ $toko->phone }}" required="">
                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-7 col-12">
                                    <label>Alamat</label>
                                    <input type="text" name ="address" class="form-control" value="{{ $toko->address }}" required="">
                                    @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
