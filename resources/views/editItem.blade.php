@extends('layouts.main-app')

@section('content')
    <div class="section-header">
        <h1>Item</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="/home">Dashboard</a></div>
            <div class="breadcrumb-item">Item</div>
        </div>
    </div>
    <div class="section-body">
        <h2 class="section-title">Hi, {{ Auth::user()->name }}</h2>
        <p class="section-lead">
            Change information about the Item.
        </p>

        <div class="row mt-sm-4">
            <div class="col">
                <div class="card">
                    <form method="post" action="/update-item" class="needs-validation">
                        @csrf
                        <input name="id" type="hidden" class="form-control" value="{{ $item->id }}" required="">
                        <div class="card-header">
                            <h4>Edit Harga {{ $item->nama_item }}</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col">
                                    <label>Harga</label>
                                    <input name="harga" type="number" class="form-control" value="{{ $item->harga }}" required="">
                                    @error('harga')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
